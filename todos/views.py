from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList
from todos.forms import TodoForm

# Create your views here.


def todo_list_list(request):
    lists = TodoList.objects.all()
    context = {"lists": lists}
    return render(request, "todo_lists/list.html", context)


def to_do_list_detail(request, id):
    item_list = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoForm(request.POST, instance=item_list)
        if form.is_valid():
            form.save()
            return redirect("todo_list_items", id=id)
    else:
        form = TodoForm(instance=item_list)

    context = {
        "item_list": item_list,
        "form": form,
    }

    return render(request, "todos/edit.html", context)
